/*
 * Copyright 2015-2018 Filippo Leonardi <filippo.leon@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include <allegro5/allegro.h>

#include <core/debug.hpp>

#define AL_CHECK(x, msg) ultima::allegro::CheckResult((x), msg, __FILE__, __LINE__);

namespace ultima::allegro {

inline void CheckResult(bool x, std::string msg, const char *file, int line) {
	if (!x) {
		BOOST_THROW_EXCEPTION(ultima::core::Exception{} << ultima::core::MessageInfo(msg));
	}
}

} // ultima::allegro