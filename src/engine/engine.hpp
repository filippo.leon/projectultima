/*
 * Copyright 2015-2018 Filippo Leonardi <filippo.leon@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include <vector>
#include <tuple>
#include <type_traits>

#include "core/id/object.hpp"
#include "core/id/vector.hpp"
#include "core/debug.hpp"

namespace ultima::engine {

namespace detail {

template<class S, int size>
constexpr int GetTupleIndexImpl() {
	static_assert(!std::is_same_v<S, S>, "Invalid component 'S'!");
	return -1;
}

template<class S, int size, class T, class ...Args>
constexpr int GetTupleIndexImpl() {
	if constexpr (std::is_same_v<S, T>) {
		return size - sizeof...(Args) - 1;
	}
	else {
		return GetTupleIndexImpl<S, size, Args...>();
	}
}

template <unsigned int N, class T, class Array, class Fun>
void RemoveComponent(const Array &id2index, const Fun &getComponentId, T &clh) {
	if constexpr (N > 1) {
		auto id = getComponentId(N - 1);
		if (!id.IsValid()) {
			std::get<N - 1>(clh).RemoveComponent(id2index[id]);
		}
		RemoveComponent<N-1>(id2index, getComponentId, clh);
	}
}

} // namespace detail

class ComponentIndex : public core::id::Object<int, -1> {
	using super::super;
};

class Entity : public core::id::Object<int, -1> {
	using super::super;
};

template <class T>
class ComponentList {
public:
	ComponentIndex CreateComponent(const T &t) {
		if (freeIndices.empty()) {
			components.push_back(t);

			return ComponentIndex((int)components.size() - 1);
		} else {
			ComponentIndex idx = freeIndices.back();
			freeIndices.resize(freeIndices.size() - 1);
			components[idx] = t;
			return idx;
		}
	}

	const core::id::Vector<ComponentIndex, T> &GetComponents() const {
		return components;
	}

	core::id::Vector<ComponentIndex, T> &GetComponents() {
		return components;
	}

	void RemoveComponent(ComponentIndex idx) {
		components[idx] = {};
		freeIndices.push_back(idx);
	}

private:
	core::id::Vector<ComponentIndex, T> components;

	std::vector<ComponentIndex> freeIndices;
};

template <class ...Args>
struct ComponentListHolder : public std::tuple<ComponentList<Args>...> {
	template <unsigned int N>
	const auto &GetComponentList() const {
		return std::get<N>(*this);
	}

	template <unsigned int N>
	auto &GetComponentList() {
		return std::get<N>(*this);
	}

	static constexpr int size = sizeof...(Args);
};

template <class ...Args>
class Engine {
public:
	class ComponentId : public core::id::FixedObject<int, sizeof...(Args)> {
		using core::id::FixedObject<int, sizeof...(Args)>::FixedObject;
	};

	template <class T>
	static constexpr ComponentId GetComponentId() {
		return ComponentId(detail::GetTupleIndexImpl<T, sizeof...(Args), Args...>());
	}

	template <class T>
	ComponentIndex GetComponentIndex(Entity e) const {
		return entity2componentIndices[e][GetComponentId<T>()];
	}

	ComponentIndex GetComponentIndex(Entity e, ComponentId id) const {
		return entity2componentIndices[e][id];
	}

	template <class T>
	auto &GetComponentList() {
		constexpr ComponentId id = GetComponentId<T>();
		return componentListHolder.template GetComponentList<id.id>();
	}

	template <class T>
	const auto &GetComponentList() const {
		constexpr ComponentId id = GetComponentId<T>();
		return componentListHolder.template GetComponentList<id.id>();
	}

	template <class T>
	T &GetComponent(Entity e) {
		return const_cast<T &>(static_cast<const Engine<Args...> &>(*this).GetComponent<T>(e));
	}

	template <class T>
	bool IsInRange(ComponentIndex index) const {
		const auto &components = GetComponentList<T>().GetComponents();
		return index.IsValid() && index.id >= 0 && index.id < (int)components.size();
	}

	template <class T>
	const T &GetComponent(Entity e) const {
		ComponentIndex index = GetComponentIndex<T>(e);
		const auto &components = GetComponentList<T>().GetComponents();
		ENSURE(index.IsValid());
		ENSURE(IsInRange<T>(index));
		return components[index];
	}

	template <class T>
	void RemoveComponent(Entity e) {
		auto index = GetComponentIndex<T>(e);
		ENSURE(index.IsValid());
		ENSURE(IsInRange<T>(index));
		GetComponentList<T>().RemoveComponent(index);

		auto id = GetComponentId<T>();
		entity2componentIndices[e][id] = {};
	}

	template <class T>
	bool HasComponent(Entity e) {
		auto index = GetComponentIndex<T>(e);
		
		return index.IsValid();
	}

	template <class T>
	void AddComponent(Entity e, const T &t) {
		auto index = GetComponentIndex<T>(e);
		ENSURE(!index.IsValid());
		auto newIndex = GetComponentList<T>().CreateComponent(t);

		auto id = GetComponentId<T>();
		entity2componentIndices[e][id] = newIndex;
	}

	Entity CreateEntity() {
		if (freeEntities.empty()) {
			entity2componentIndices.push_back({});
			
			return Entity((int)entity2componentIndices.size() - 1);
		} else {
			Entity e = freeEntities.back();
			freeEntities.resize(freeEntities.size() - 1);
			return e;
		}
	}

	void RemoveEntity(Entity &&e) {
		detail::RemoveComponent<ComponentListHolder<Args...>::size>(entity2componentIndices[e], 
			[] (unsigned int N) { return ComponentId(N); }, 
			componentListHolder
		);

		entity2componentIndices[e] = {};
		freeEntities.push_back(e);
	}

private:
	core::id::Vector<Entity, core::id::Array<ComponentId, ComponentIndex>> entity2componentIndices;

	std::vector<Entity> freeEntities;

	ComponentListHolder<Args...> componentListHolder;
};

} // namespace ultima::engine