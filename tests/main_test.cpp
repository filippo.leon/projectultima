/*
 * Copyright 2015-2018 Filippo Leonardi <filippo.leon@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include <allegro/utils.hpp>

int main() {
	AL_CHECK(al_init(), "Failed to initialize!");

	constexpr float fps = 60.0f;
	ALLEGRO_TIMER *timer = al_create_timer(1.0f / fps);
	AL_CHECK(timer, "Couldn't create timer.");

	ALLEGRO_DISPLAY *display = al_create_display(640, 480);
	AL_CHECK(display, "Couldn't create display.");

	ALLEGRO_EVENT_QUEUE *event_queue = al_create_event_queue();
	AL_CHECK(event_queue, "Failed to create event queue.");

	al_register_event_source(event_queue, al_get_display_event_source(display));
	al_register_event_source(event_queue, al_get_timer_event_source(timer));

	al_clear_to_color(al_map_rgb(0, 0, 0));
	al_flip_display();

	al_start_timer(timer);

	bool running = true;
	bool redraw = true;
	while (running) {
		ALLEGRO_TIMEOUT timeout;
		al_init_timeout(&timeout, 0.06);

		ALLEGRO_EVENT event;
		bool get_event = al_wait_for_event_until(event_queue, &event, &timeout);

		if (get_event) {
			switch (event.type) {
			case ALLEGRO_EVENT_TIMER:
				redraw = true;
				break;
			case ALLEGRO_EVENT_DISPLAY_CLOSE:
				running = false;
				break;
			default:
				BOOST_THROW_EXCEPTION(ultima::core::Exception{} << ultima::core::MessageInfo("Unhandled event!"));
				break;
			}
		}

		if (redraw && al_is_event_queue_empty(event_queue)) {
			al_clear_to_color(al_map_rgb(0, 0, 0));
			al_flip_display();
			redraw = false;
		}
	}

	al_destroy_display(display);
	al_destroy_event_queue(event_queue);
}