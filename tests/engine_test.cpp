#include <iostream>
#include <cmath>

#define BOOST_TEST_MODULE engine_test
#include <boost/test/unit_test.hpp>

#include "core/math/vec.hpp"
#include "engine/engine.hpp"

namespace ultima::engine {

struct S1 {};
struct S2 {};
struct S3 {
	int data;
};

} // namespace ultima::engine

BOOST_AUTO_TEST_CASE(engine_test) {
	{
		ultima::engine::Engine<ultima::engine::S1, ultima::engine::S2> eng;

		BOOST_CHECK_EQUAL(eng.GetComponentId<ultima::engine::S1>().id, 0);
		BOOST_CHECK_EQUAL(eng.GetComponentId<ultima::engine::S2>().id, 1);

		auto e1 = eng.CreateEntity();
		auto e2 = eng.CreateEntity();
		auto e3 = eng.CreateEntity();
		auto e4 = eng.CreateEntity();

		eng.RemoveEntity(std::move(e3));
		auto e5 = eng.CreateEntity();
		auto e6 = eng.CreateEntity();

		BOOST_CHECK_EQUAL(e1.id, 0);
		BOOST_CHECK_EQUAL(e2.id, 1);
		BOOST_CHECK_EQUAL(e3.id, 2);
		BOOST_CHECK_EQUAL(e4.id, 3);
		BOOST_CHECK_EQUAL(e5.id, 2);
		BOOST_CHECK_EQUAL(e6.id, 4);

		eng.RemoveEntity(std::move(e1));
		eng.RemoveEntity(std::move(e2));
		eng.RemoveEntity(std::move(e4));
		auto e7 = eng.CreateEntity();
		BOOST_CHECK_EQUAL(e7.id, 3);
	}

	{
		ultima::engine::Engine<ultima::engine::S1, ultima::engine::S2, ultima::engine::S3> eng3;
		auto E1 = eng3.CreateEntity();
		auto E2 = eng3.CreateEntity();

		eng3.AddComponent(E1, ultima::engine::S3{});
		eng3.AddComponent(E2, ultima::engine::S2{});

		eng3.AddComponent(E1, ultima::engine::S2{});

		const ultima::engine::S2 &s2 = eng3.GetComponent<ultima::engine::S2>(E2);
		const auto &s3 = eng3.GetComponent<ultima::engine::S2>(E1);

		BOOST_CHECK(eng3.HasComponent<ultima::engine::S2>(E1));
		BOOST_CHECK(eng3.HasComponent<ultima::engine::S2>(E2));

		BOOST_CHECK(eng3.HasComponent<ultima::engine::S3>(E1));

		eng3.RemoveComponent<ultima::engine::S2>(E1);
		eng3.RemoveComponent<ultima::engine::S2>(E2);

		BOOST_CHECK(!eng3.HasComponent<ultima::engine::S2>(E1));
		BOOST_CHECK(!eng3.HasComponent<ultima::engine::S2>(E1));

		BOOST_CHECK(eng3.HasComponent<ultima::engine::S3>(E1));

		eng3.RemoveEntity(std::move(E1));
		eng3.RemoveEntity(std::move(E2));

		auto E3 = eng3.CreateEntity();

		eng3.AddComponent(E3, ultima::engine::S1{});

		BOOST_CHECK(eng3.HasComponent<ultima::engine::S1>(E3));
		BOOST_CHECK(!eng3.HasComponent<ultima::engine::S2>(E3));
		BOOST_CHECK(!eng3.HasComponent<ultima::engine::S3>(E3));
	}
}