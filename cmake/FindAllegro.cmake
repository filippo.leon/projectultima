# Copyright 2018 Filippo Leonardi
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#.rst:
# FindAllegro
# -------
#
# Finds the Allegro 5 library and iuts components
#
# This will use the following optional varables as inputs::
#
#	ALLEGRO_ROOT
#
# This will define the following variables::
#
#   Allegro_FOUND    - True if the system has the Allegro library
#   Allegro_VERSION  - The version of the Allegro library which was found
#
# and the following imported targets::
#
#   Allegro::Allegro        - All requested components library
#   Allegro::${component}   - The Allegro ${component} library

find_package(PkgConfig QUIET)
pkg_check_modules(PC_Allegro QUIET Allegro)

find_path(Allegro_INCLUDE_DIR
	NAMES allegro5/allegro.h
	PATHS ${PC_Allegro_INCLUDE_DIRS} ${ALLEGRO_ROOT}/include
)
find_library(Allegro_LIBRARY
	NAMES allegro-debug
	PATHS ${PC_Allegro_LIBRARY_DIRS} ${ALLEGRO_ROOT}/lib/
)

if(Allegro_INCLUDE_DIR AND Allegro_LIBRARY) 
	message(STATUS ${Allegro_LIBRARY})
	message(STATUS ${Allegro_INCLUDE_DIR})
endif()

if(PC_Allegro_VERSION)
	set(Allegro_VERSION ${PC_Allegro_VERSION})
else()
	if(EXISTS "${Allegro_main_INCLUDE_DIR}/base.h")
		file(STRINGS ${Allegro_INCLUDE_DIR}/base.h
			Allegro_VERSION_MAJOR
			REGEX "#define ALLEGRO_VERSION "
		)
		file(STRINGS ${Allegro_INCLUDE_DIR}/base.h
			Allegro_VERSION_MINOR
			REGEX "#define ALLEGRO_SUB_VERSION "
		)
		file(STRINGS ${Allegro_INCLUDE_DIR}/base.h
			Allegro_VERSION_SUB
			REGEX "#define ALLEGRO_WIP_VERSION "
		)
		string(REGEX REPLACE "#define ALLEGRO_VERSION[ \t]+([0-9]+)" "\\1" Allegro_VERSION_MAJOR "${Allegro_VERSION_MAJOR}")
		string(REGEX REPLACE "#define ALLEGRO_SUB_VERSION[ \t]+([0-9]+)" "\\1" Allegro_VERSION_MINOR "${Allegro_VERSION_MINOR}")
		string(REGEX REPLACE "#define ALLEGRO_WIP_VERSION[ \t]+([0-9]+)" "\\1" Allegro_VERSION_SUB "${Allegro_VERSION_SUB}")
		set(Allegro_VERSION ${Allegro_VERSION_MAJOR}.${Allegro_VERSION_MINOR}.${Allegro_VERSION_SUB})
		message(STATUS "Found Allegro version ${Allegro_VERSION}")
	endif()
endif()

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Allegro
	FOUND_VAR Allegro_FOUND
	REQUIRED_VARS
		Allegro_LIBRARY
		Allegro_INCLUDE_DIR
	VERSION_VAR Allegro_VERSION
)

if(Allegro_FOUND AND NOT TARGET Allegro::base)
	add_library(Allegro::base UNKNOWN IMPORTED)
	set_target_properties(Allegro::base PROPERTIES
		IMPORTED_LOCATION ${Allegro_LIBRARY}
		INTERFACE_COMPILE_OPTIONS "${PC_Allegro_CFLAGS_OTHER}"
		INTERFACE_INCLUDE_DIRECTORIES ${Allegro_INCLUDE_DIR}
	)
endif()

foreach(comp ${Allegro_FIND_COMPONENTS})
	message(STATUS "Looking for component '${comp}'")
	find_library(Allegro_${comp}_LIBRARY
		NAMES allegro_${comp}-debug
		PATHS ${PC_Allegro_LIBRARY_DIRS} ${ALLEGRO_ROOT}/lib/
	)
	if(Allegro_INCLUDE_DIR AND Allegro_${comp}_LIBRARY)
		message(STATUS "Found component '${comp}'")
		message(STATUS ${Allegro_${comp}_LIBRARY})
	else()
		message(WARNING "Cannot find component '${comp}'")
	endif()

	if(NOT TARGET Allegro::${comp})
		add_library(Allegro::${comp} UNKNOWN IMPORTED)
		set_target_properties(Allegro::${comp} PROPERTIES
			IMPORTED_LOCATION ${Allegro_${comp}_LIBRARY}
			IMPORTED_LINK_INTERFACE_LIBRARIES Allegro::base
			INTERFACE_COMPILE_OPTIONS "${PC_Allegro_CFLAGS_OTHER}"
			INTERFACE_INCLUDE_DIRECTORIES ${Allegro_INCLUDE_DIR}
		)
	endif()

	list(APPEND Allegro_LIBRARY ${Allegro_${comp}_LIBRARY})
	list(APPEND Allegro_TARGETS ${comp})
endforeach()

if(Allegro_FOUND)
	set(Allegro_LIBRARIES ${Allegro_LIBRARY})
	set(Allegro_INCLUDE_DIRS ${Allegro_INCLUDE_DIR})
	set(Allegro_DEFINITIONS ${PC_Allegro_CFLAGS_OTHER})
endif()

if(Allegro_FOUND AND NOT TARGET Allegro::Allegro)
	add_library(Allegro::Allegro UNKNOWN IMPORTED)
	foreach(comp ${Allegro_TARGETS})
		target_link_libraries(Allegro::Allegro INTERFACE Allegro::${comp})
	endforeach()
endif()

mark_as_advanced(
	Allegro_INCLUDE_DIR
	Allegro_LIBRARY
)